import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CategoriPlatosPipe } from '../pipe/categori-platos.pipe';
// Tables
import { TablaClienteComponent } from './tablas/tabla-cliente/tabla-cliente.component';
import { TableCatgoriaComponent } from './tablas/tabla-categoria/table-categoria.component';
import { TablaPlatoComponent } from './tablas/tabla-platos/tabla-plato.component';
// Formularios
import { CrearClienteComponent } from './formularios/crear/cliente/crear-cliente.component';
import { CrearCategoriaComponent } from './formularios/crear/categoria/crear-categoria.component';
import { CrearPlatoComponent } from './formularios/crear/plato/crear-plato.component';
import { ActualizarClienteComponent } from './formularios/actualizar/cliente/actualizar-cliente.component';
import { ActualizarCategoriaComponent } from './formularios/actualizar/categoria/actualizar-categoria.component';
import { ActualizarPlatoComponent } from './formularios/actualizar/plato/actualizar-plato.component';
import { ActivadoPipe } from '../pipe/activado.pipe';
import { RolPipe } from '../pipe/rol.pipe';

@NgModule({
    declarations: [
        TablaClienteComponent,
        TableCatgoriaComponent,
        TablaPlatoComponent,
        CrearClienteComponent,
        CrearCategoriaComponent,
        CrearPlatoComponent,
        ActualizarClienteComponent,
        ActualizarCategoriaComponent,
        ActualizarPlatoComponent,
        CategoriPlatosPipe,
        ActivadoPipe,
        RolPipe
    ],
    imports: [ CommonModule, FormsModule, ReactiveFormsModule ],
    exports: [
        TablaClienteComponent,
        TableCatgoriaComponent,
        TablaPlatoComponent,
        CrearClienteComponent,
        CrearCategoriaComponent,
        CrearPlatoComponent,
        ActualizarClienteComponent,
        ActualizarCategoriaComponent,
        ActualizarPlatoComponent,
        CategoriPlatosPipe
    ],
    providers: [],
})
export class ComponentesModule {}