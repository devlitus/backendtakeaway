import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Categoria } from 'src/app/models/categoria';
import { FormGroup, FormControl } from '@angular/forms';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
declare var swal: any;

@Component({
  selector: 'app-actualizar-categoria',
  templateUrl: './actualizar-categoria.component.html',
  // styleUrls: ['./actualizar-categoria.component.css']
})
export class ActualizarCategoriaComponent implements OnInit, OnChanges {
  @Input() categoria: Categoria;
  public actualizarCategoria: FormGroup;
  public isDisabled = true;
  public cat: Categoria = {
    categoria: '',
    descripcion: '',
  };
  constructor(private _service: CategoriaService) { }

  ngOnInit(): void { }
  ngOnChanges(): void {
    if (this.categoria !== undefined) {
      this.cat = this.categoria;
      this.isDisabled = false;
    }
  }
  onSubmit(actualizarCategoria: FormControl) {
    if (actualizarCategoria.valid) {
      let id = actualizarCategoria.value.id;
      this._service.putCategoria(id, actualizarCategoria.value)
      .subscribe(res => {
        if (res.ok) {
          swal('Buen trabajo', `${res.message}`, 'success');
        } else {
          swal('Oooppss', `${res.message}`, 'error');
        }
      });
    }
    // console.log(actualizarCategoria.value.id);
  }
}
