import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { Cliente } from 'src/app/models/cliente';
import { FormGroup } from '@angular/forms';
import { URL_IMAGEN_CLIENTE } from 'src/app/config/config';
import { Router } from '@angular/router';
declare var swal: any;
@Component({
  selector: 'app-actualizar-cliente',
  templateUrl: './actualizar-cliente.component.html',
  // styleUrls: ['./actualizar-cliente.component.css']
})
export class ActualizarClienteComponent implements OnInit, OnChanges {
  @Input() user: Cliente;
  public datos = [];
  actform: FormGroup;
  actFomrImg: FormGroup;
  isDisable = true;
  urlImagen = URL_IMAGEN_CLIENTE;
  constructor(public _service: ClienteService, private router: Router) { }

  ngOnInit(): void {

  }
  ngOnChanges(): void {
    if (this.user !== undefined) {
      this.isDisable = false;
    }
  }

  onSubmit(actform: FormGroup) {
    this._service.putCliente(actform.value, actform.value.id)
    .subscribe(res => {
      if (res.ok) {
        swal('Buen trabajo', `${res.message}`, 'success');
        this.router.navigate(['/cliente']);
      } else {
        swal('Oooppss', `${res.message}`, 'error');
      }
      console.log(res);
    });
  }
  actFormImagen(event, actFomrImg: FormGroup) {
    let id = actFomrImg.value.idIm;
    let form = new FormData();
    form.append('imagen', event.target.file.files[0]);
    this._service.postImagen(id, form)
    .subscribe(res => {
      if (res.ok) {
        swal('Buen trabajo', `${res.message}`, 'success');
        this.router.navigate(['/cliente']);
      } else {
        swal('Oooppss', `${res.message}`, 'error');
      }
    });
  }

}
