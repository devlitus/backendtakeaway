import { Component, OnInit, Output, Input, OnChanges } from '@angular/core';
import { Plato } from 'src/app/models/plato';
import { FormGroup, FormControl } from '@angular/forms';
import { URL_IMAGEN } from 'src/app/config/config';
import { PlatoService } from 'src/app/services/plato/plato.service';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
declare var swal: any;
@Component({
  selector: 'app-actualizar-plato',
  templateUrl: './actualizar-plato.component.html',
  // styleUrls: ['./actualizar-plato.component.css']
})
export class ActualizarPlatoComponent implements OnInit, OnChanges {
  @Input() dataPlato: Plato;
  public isDisabled: boolean = true;
  public actualizarForm: FormGroup;
  public atcplIm: FormGroup;
  public urlImagenPlato: any = URL_IMAGEN;
  public plato: Plato = {
    nombre: '',
    descripcion: '',
    precio: 0,
    imagen: '',
    activado: true,
    id_categoria: 0,
  };
  constructor(private _service: PlatoService) { }

  ngOnInit(): void {  }
  ngOnChanges(): void {
    if (this.dataPlato !== undefined) {
      this.plato = this.dataPlato;
      this.isDisabled = false;
      // console.log(this.dataPlato);
    }
  }
  onSubmit(formulario: FormControl) {
    let id = formulario.value.id;
    this._service.putPlato(id, formulario.value)
    .subscribe((res: any) => {
      if (res.ok) {
        swal('Buen trabajo', `${res.message}`, 'success');
        // this.router.navigate(['/cliente']);
      } else {
        swal('Oooppss', `${res.message}`, 'error');
      }
    });
    // console.log(formulario.value);
  }
  actForm(imagen: any, actFormImg: FormControl) {
    let id = actFormImg.value.id;
    // console.log(id);
    let form = new FormData();
    form.append('imagen', imagen.target.file.files[0]);
    this._service.postImagen(id, form)
    .subscribe((res: any) => {
      if (res.ok) {
        swal('Buen trabajo', `${res.message}`, 'success');
        // this.router.navigate(['/cliente']);
      } else {
        swal('Oooppss', `${res.message}`, 'error');
      }
    });
    console.log(imagen.target.file.files[0]);
  }
}
