import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
declare var swal: any;

@Component({
  selector: 'app-crear-categoria',
  templateUrl: './crear-categoria.component.html',
  // styleUrls: ['./categoria.component.css']
})
export class CrearCategoriaComponent implements OnInit {
  public createForm = new FormGroup({
    categoria: new FormControl('', Validators.required),
    descripcion: new FormControl('')
  });
  constructor(private _service: CategoriaService) { }

  ngOnInit(): void { }
  onSubmit() {
    if (this.createForm.valid) {
      this._service.postCategoria(this.createForm.value)
      .subscribe(res => {
        if (res.ok) {
          swal('Buen trabajo', `${res.message}`, 'success');
        } else {
          swal('Oooppss', `${res.message}`, 'error');
        }
        console.log(res);
      });
      // console.log(this.createForm.value);
    }
  }
}
