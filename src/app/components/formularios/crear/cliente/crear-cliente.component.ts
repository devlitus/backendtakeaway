import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/models/cliente';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
declare var swal: any;
@Component({
  selector: 'app-crear-cliente',
  templateUrl: './crear-cliente.component.html',
  // styleUrls: ['./crear.component.css']
})
export class CrearClienteComponent implements OnInit {

  public clienteForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
    role: new FormControl('USER_ROLE'),
    activado: new FormControl('activado')
  });
  constructor(public _service: ClienteService) { }

  ngOnInit(): void {

  }
  onSubmit(event: any) {
    if (this.clienteForm.valid) {
      const cliente: Cliente = this.clienteForm.value;
      const form = new FormData();
      form.append('nombre', cliente.nombre);
      form.append('email', cliente.email);
      form.append('password', cliente.password);
      form.append('role', cliente.role);
      form.append('activado', cliente.activado);
      form.append('imagen', event.target.imagen.files[0]);
      this._service.postCliente(form)
      .subscribe(res => {
        if (res.ok) {
          swal('Buen trabajo', `${res.message}`, 'success');
        } else {
          swal('Oooppss', `${res.message}`, 'error');
        }
      });
    }
  }
}
