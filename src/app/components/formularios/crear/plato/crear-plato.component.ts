import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PlatoService } from 'src/app/services/plato/plato.service';
declare var swal: any;

@Component({
  selector: 'app-crear-plato',
  templateUrl: './crear-plato.component.html',
  // styleUrls: ['./crear-palto.component.css']
})
export class CrearPlatoComponent implements OnInit {
  public platoForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    precio: new FormControl('', [Validators.required, Validators.pattern('[0-9]*')]),
    descripcion: new FormControl(''),
    id_categoria: new FormControl(''),
    activado: new FormControl(true)
  });
  constructor(private _service: PlatoService) { }

  ngOnInit(): void { }
  onSubmit(event: any) {
    if (this.platoForm.valid) {
      let form = new FormData();
      form.append('nombre', this.platoForm.value.nombre);
      form.append('descripcion', this.platoForm.value.descripcion);
      form.append('precio', this.platoForm.value.precio);
      form.append('id_categoria', this.platoForm.value.id_categoria);
      form.append('imagen', event.target.file.files[0]);
      this._service.postPlato(form)
      .subscribe(res => {
        if (res.ok) {
          swal('Buen trabajo', `${res.message}`, 'success');
        } else {
          swal('Oooppss', `${res.message}`, 'error');
        }
      });
      // console.log(event.target.file.files[0]);
    }
  }
}
