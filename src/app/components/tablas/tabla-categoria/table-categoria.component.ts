import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import { Categoria } from 'src/app/models/categoria';
import { LANGUAGE_SPANISH } from 'src/app/config/config';


declare var $: any;

@Component({
  selector: 'app-tabla-categoria',
  templateUrl: './table-categoria.component.html',
  // styleUrls: ['./categoria.component.css']
})
export class TableCatgoriaComponent implements OnInit {
  @Output() public dataCategory = new EventEmitter<Categoria>();
  public id: string = 'tableCategoria';
  public categorias: Array<Categoria> = [];
  constructor(public _service: CategoriaService) { }

  ngOnInit(): void { this.showCatgoria(); }
  showCatgoria() {
    this._service.getCategoria()
    .subscribe(res => {
      this.categorias = res;
      this.sourceData();
      // console.log(res);
    });
  }
  sourceData() {
    $(document).ready(function () {
      $('#tableCategoria').DataTable({
        language: LANGUAGE_SPANISH,
      });
    });
  }
  dataCategoria(data: Categoria) {
    this.dataCategory.emit(data);
  }
}
