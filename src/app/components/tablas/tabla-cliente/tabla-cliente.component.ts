import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { LANGUAGE_SPANISH, URL_IMAGEN_CLIENTE } from 'src/app/config/config';
import { Cliente } from 'src/app/models/cliente';

declare var $: any;
@Component({
  selector: 'app-tabla-cliente',
  templateUrl: './tabla-cliente.component.html',
  styles: ['']
})
export class TablaClienteComponent implements OnInit {
  @Output() public dataCLiente = new EventEmitter<Cliente>();
  public clientes: Array<Cliente> = [];
  public id: string = 'dataTableCliente';
  public url_imagen_cliente = URL_IMAGEN_CLIENTE;
  constructor(public _ServiceCliente: ClienteService) { }
  ngOnInit(): void {
    this.showCliente();
  }
  showCliente() {
    this._ServiceCliente.getClientes()
    .subscribe(res => {
      this.clientes = res;
      // console.log(res);
      this.sourceDatatables();
    });
  }
  sourceDatatables() {
    $(document).ready(function() {
      $('#dataTableCliente').DataTable({
        language: LANGUAGE_SPANISH,
      });
    });
  }
  dataTable(table: Cliente) {
    this.dataCLiente.emit(table);
  }
}


