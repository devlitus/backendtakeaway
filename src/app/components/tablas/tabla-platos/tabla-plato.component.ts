import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PlatoService } from 'src/app/services/plato/plato.service';
import { Plato } from 'src/app/models/plato';
import { LANGUAGE_SPANISH, URL_IMAGEN } from 'src/app/config/config';
import { Categoria } from 'src/app/models/categoria';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';

declare var $: any;

@Component({
  selector: 'app-tabla-plato',
  templateUrl: './tabla-plato.component.html',
  // styleUrls: ['./tabla-plato.component.css']
})
export class TablaPlatoComponent implements OnInit {
  @Output() dataTable = new EventEmitter<Plato>();
  public id: string = 'tablePlato';
  public platos: Array<Plato> = [];
  public url_imagen = URL_IMAGEN;
  constructor(public _service: PlatoService) { }

  ngOnInit(): void {
  this.showPlato();
  }
  showPlato() {
    this._service.getPlato()
    .subscribe(plato => {
      this.platos = plato;
      this.sourceDataTable();
    });
  }
  sourceDataTable() {
    $(document).ready(function() {
      $('#tablePlato').DataTable({
        language: LANGUAGE_SPANISH,
      });
    });
  }
  sourceData(plato: any) {
    this.dataTable.emit(plato);
    // console.log(plato);
  }
}
