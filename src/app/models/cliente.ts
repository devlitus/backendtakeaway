export class Cliente {
  nombre: string;
  email: string;
  password: string;
  imagen?: string;
  fecha?: string;
  activado?: string;
  role?: string;
  id?: number;
}