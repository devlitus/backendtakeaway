export class Plato {
  nombre: string;
  descripcion: string;
  precio: number;
  imagen?: string;
  activado?: boolean;
  id_categoria?: number;
  id?: number;
}