import { Component, OnInit } from '@angular/core';
import { Categoria } from 'src/app/models/categoria';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {
  public categoria: Categoria;
  constructor() { }

  ngOnInit() {
  }
  getCategoria(categoria: Categoria) {
    this.categoria = categoria;
    // console.log(event);
  }
}
