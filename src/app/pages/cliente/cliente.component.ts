import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/models/cliente';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
  public user: Cliente;
  constructor() { }

  ngOnInit() {
  }
  getCliente(cliente: Cliente) {
    // console.log(this.user);
    return this.user = cliente;
  }
}
