import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClienteComponent } from './cliente/cliente.component';
import { PedidoComponent } from './pedido/pedido.component';
import { PlatoComponent } from './plato/plato.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { NotPageFoundComponent } from '../shared/not-page-found/not-page-found.component';

const routes: Routes = [{
    path: '',
    component: PagesComponent,
    children: [
        {path: 'dashboard', component: DashboardComponent, data: {title: 'Dashboard', icon: 'app-menu__icon fa fa-dashboard'}},
        {path: 'categoria', component: CategoriaComponent, data: {title: 'Categoría', icon: 'app-menu__icon fa fa-th-list'}},
        {path: 'cliente', component: ClienteComponent, data: {title: 'Cliente', icon: 'app-menu__icon fa fa-users'}},
        {path: 'pedido', component: PedidoComponent, data: {title: 'Pedido', icon: 'app-menu__icon fa fa-truck'}},
        {path: 'plato', component: PlatoComponent, data: {title: 'Plato', icon: 'app-menu__icon fa fa-apple'}},
        {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
        {path: '**', component: NotPageFoundComponent}
    ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
