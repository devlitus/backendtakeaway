import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { SharedModule } from '../shared/shared.module';


import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClienteComponent } from './cliente/cliente.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { PedidoComponent } from './pedido/pedido.component';
import { PlatoComponent } from './plato/plato.component';
import { ComponentesModule } from '../components/componente.module';

@NgModule({
    declarations: [
        PagesComponent,
        DashboardComponent,
        ClienteComponent,
        CategoriaComponent,
        PedidoComponent,
        PlatoComponent,
    ],
    imports: [ CommonModule, SharedModule, PagesRoutingModule, ComponentesModule ],
    exports: [],
    providers: [],
})
export class PagesModule {}
