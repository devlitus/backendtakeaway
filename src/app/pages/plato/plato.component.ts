import { Component, OnInit } from '@angular/core';
import { Plato } from 'src/app/models/plato';

@Component({
  selector: 'app-plato',
  templateUrl: './plato.component.html',
  styleUrls: ['./plato.component.css']
})
export class PlatoComponent implements OnInit {
  public dataPlato: Plato;
  constructor() { }

  ngOnInit() {
  }
  getTable(plato: Plato) {
    return this.dataPlato = plato;
    // console.log(plato);
  }

}
