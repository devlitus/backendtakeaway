import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'activado'
})
export class ActivadoPipe implements PipeTransform {

  transform(value: any): any {
    switch (value) {
      case '1':
        return 'Activado';
        break;
      case '0':
        return 'Desactivado';
        break;
      default:
        return 'Activado';
        break;
    }
  }

}
