import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'categoriPlatos'
})
export class CategoriPlatosPipe implements PipeTransform {

  transform(value: any): any {
    switch (value) {
      case '1':
        return 'Entrantes';
        break;
      case '2':
        return 'Primeros';
        break;
      case '3':
        return 'Segundos';
        break;
      case '4':
        return 'Postres';
        break;
      default:
        return 'No lo se';
        break;
    }
  }

}
