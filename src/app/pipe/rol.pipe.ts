import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rol'
})
export class RolPipe implements PipeTransform {

  transform(value: any): any {
    switch (value) {
      case 'USER_ROLE':
        return 'Cliente';
        break;
      case 'ADMIN_ROLE':
        return 'Administrador';
        break;
      default:
        return 'USER_ROLE';
        break;
    }
  }

}
