import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICE } from 'src/app/config/config';
import { map } from 'rxjs/operators';
import { Categoria } from 'src/app/models/categoria';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  constructor(private _http: HttpClient) {}
  getCategoria() {
    return this._http.get(URL_SERVICE + '/categoria')
    .pipe(map((res: any) => {
      return res.categoria;
    }));
  }
  postCategoria(categoria: Categoria): Observable<any> {
    return this._http.post(URL_SERVICE + '/categoria', categoria)
    .pipe(map((res: any) => {
      return res;
    }));
  }
  putCategoria(id: number, categoria: Categoria): Observable<any> {
    return this._http.put(URL_SERVICE + `/categoria/${id}`, categoria)
    .pipe(map((res: any) => {
      return res;
    }));
  }
}