import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { URL_SERVICE } from 'src/app/config/config';
import { map } from 'rxjs/operators';
import { Cliente } from 'src/app/models/cliente';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  public tableDataCliente: Cliente;
  public observer: Observable<any>;
  constructor(public _http: HttpClient) { }

  getClientes(): Observable<any> {
    return this._http.get(URL_SERVICE + '/cliente')
    .pipe(map((res: any) => {
      if (res.ok) {
        return res.usuario;
      }
      return res.message;
    }));
  }
  postCliente(cliente: any): Observable<any> {
    return this._http.post(URL_SERVICE + '/cliente', cliente)
    .pipe(map((res: any) => {
      return res;
    }));
  }
  postImagen(id: number, cliente: any): Observable<any> {
    console.log(id);
    return this._http.post(URL_SERVICE + `/imageCliente/${id}`, cliente)
    .pipe(map((res: any) => {
      return res;
    }));
  }
  putCliente(cliente: any, id: any): Observable<any> {
    console.log(id);
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Headers': 'application/x-www-form-urlencoded',
        'Content-Type': 'application/json;charset=utf-8',
      })
    };
    return this._http.put(URL_SERVICE + `/cliente/${id}`, cliente, httpOptions)
    .pipe(map((res: any) => {
      return res;
    }));
  }
  tableCliente(data: any) {
    this.tableDataCliente = data;
    if (data !== undefined) {
    }
  }
}
