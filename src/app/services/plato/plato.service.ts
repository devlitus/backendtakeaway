import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICE } from 'src/app/config/config';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Plato } from 'src/app/models/plato';

@Injectable({
  providedIn: 'root'
})
export class PlatoService {
  constructor(private _http: HttpClient) {}
  getPlato() {
    return this._http.get(URL_SERVICE + '/plato')
    .pipe(map((res: any) => {
      return res.plato;
    }));
  }
  postPlato(plato: any): Observable<any> {
    return this._http.post(URL_SERVICE + '/plato', plato)
    .pipe(map((respons: any) => {
      return respons;
    }))
    ;
  }
  postImagen(id: number, imagen: any): Observable<any> {
    console.log(id);
    return this._http.post(URL_SERVICE + `/imagePlato/${id}`, imagen)
    .pipe(map((res: any) => {
      return res;
    }));
  }
  putPlato(id: number, plato: Plato): Observable<any> {
    return this._http.put(URL_SERVICE + `/plato/${id}`, plato)
    .pipe(map((res: any) => {
      return res;
    }));
  }
}