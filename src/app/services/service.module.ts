import { NgModule } from '@angular/core';
import { SidebarService } from './sidebar/sidebar.service';
import { ClienteService } from './cliente/cliente.service';

@NgModule({
    providers: [SidebarService, ClienteService],
})
export class ServiceModule {}
