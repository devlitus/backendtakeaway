import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  constructor() { }
  menu: any = [
    {
      titulo: 'Dashboard',
      icono: 'app-menu__icon fa fa-dashboard',
      url: '/dashboard'
    },
    {
      titulo: 'Clientes',
      icono: 'app-menu__icon fa fa-users',
      url: '/cliente'
    },
    {
      titulo: 'Categorias',
      icono: 'app-menu__icon fa fa-th-list',
      url: '/categoria'
    },
    {
      titulo: 'Pedidos',
      icono: 'app-menu__icon fa fa-truck',
      url: '/pedido'
    },
    {
      titulo: 'Platos',
      icono: 'app-menu__icon fa fa-apple',
      url: '/plato'
    },
  ];
}
