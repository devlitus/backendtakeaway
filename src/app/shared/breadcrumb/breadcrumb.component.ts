import { Component, OnInit } from '@angular/core';
import { Router, ActivationEnd } from '@angular/router';
import { Title, Meta, MetaDefinition } from '@angular/platform-browser';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {

  public titulo: string = 'Dashboard';
  public icon: string = 'app-menu__icon fa fa-dashboard';
  constructor(private router: Router, private title: Title, private meta: Meta) { }

  ngOnInit() {
    this.getDataRouter();
  }
  getDataRouter() {
    this.router.events.pipe(
      filter(evento => evento instanceof ActivationEnd),
      filter((evento: ActivationEnd) => evento.snapshot.firstChild === null),
      map((evento: ActivationEnd) => evento.snapshot.data)
    )
    .subscribe(data => {
      // console.log(data.icon);
      this.titulo = data.title;
      this.icon = data.icon;
      this.title.setTitle(data.title);

      const metaTag: MetaDefinition = {
        name: 'description',
        content: this.titulo
      };
      this.meta.addTag(metaTag);
    });
  }
}
