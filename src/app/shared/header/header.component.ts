import { Component, OnInit, ElementRef } from '@angular/core';

declare var $: any;

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    constructor(public el: ElementRef) { }

    ngOnInit(): void {
        $(document).ready(function() {
            // Toggle Sidebar
            $('[data-toggle="sidebar"]').click(function(event) {
              event.preventDefault();
              $('.app').toggleClass('sidenav-toggled');
            });
          });
     }
}
