import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { NotPageFoundComponent } from './not-page-found/not-page-found.component';

@NgModule({
    declarations: [HeaderComponent, SidebarComponent, BreadcrumbComponent, NotPageFoundComponent],
    imports: [ CommonModule, RouterModule ],
    exports: [HeaderComponent, SidebarComponent, BreadcrumbComponent, NotPageFoundComponent],
    providers: [],
})
export class SharedModule {}
